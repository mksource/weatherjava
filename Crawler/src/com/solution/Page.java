package com.solution;

public class Page {
	
	//Url of the web page
	private String      url;
		
	//Status Code that was returned 
	private String      statusCode;
	 
	//contenttype that was fetched for this page
	private String      contentType;
	
	//Encoding for this page
	private String      contentEncoding;
	
	//Character Set for this page
	private String      contenCharSet;
	
	//Language for this page
	private String      language;
	
	//Response for this page
	private String      response;

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getContentEncoding() {
		return contentEncoding;
	}

	public void setContentEncoding(String contentEncoding) {
		this.contentEncoding = contentEncoding;
	}

	public String getContenCharSet() {
		return contenCharSet;
	}

	public void setContenCharSet(String contenCharSet) {
		this.contenCharSet = contenCharSet;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}
	
	
	
	
	

}

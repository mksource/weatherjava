package com.solution.service;

import java.io.File;
import java.util.LinkedList;
import java.util.List;
import java.util.NavigableSet;
import java.util.concurrent.ConcurrentNavigableMap;

import org.mapdb.BTreeKeySerializer;
import org.mapdb.DB;
import org.mapdb.DBMaker;
import org.mapdb.Fun;


public class CacheService {
	
	private  ConcurrentNavigableMap<String,String> pinToSecondaryMap;
	
	private ConcurrentNavigableMap<String,String> secondaryToPin;
	
	private  NavigableSet<Fun.Tuple2<String, String>> secondaryToUsers;
	
	private static CacheService cacheService;
	
	private DB db;
	
			
	private CacheService(){
		
		DB db=DBMaker.newFileDB(new File("pindb")).closeOnJvmShutdown().make();
		
		pinToSecondaryMap=db.getTreeMap("pinToSecondaryMap");
		secondaryToPin=db.getTreeMap("secondaryToPin");
		secondaryToUsers=db.createTreeSet("secondaryToUsers")
        .serializer(BTreeKeySerializer.TUPLE2)
        .make();
		
	}
	
	//Construct a Singleton for the CacheService
	public static CacheService getCacheService(){
		
		if(cacheService == null){
			
			synchronized(CacheService.class){
				
				if(cacheService == null){
					
					cacheService=new CacheService();
				}
				
			}
		}
			
			
		return cacheService;
		
	}
	
		
	//add secondaryId to a PIN 
	public boolean putSecondaryForPin(String pin,String secondaryId){
		
		
		if(pinToSecondaryMap.containsKey(secondaryId))
			return false;
		pinToSecondaryMap.put(secondaryId,pin);
		secondaryToPin.put(pin, secondaryId);
		
		return true;
	}
	
	//get a secondary for Pin 
	public String getSecondaryForPin(String pin){
		
		String secondary_id=null;
		if(secondaryToPin.containsKey(pin))
			secondary_id=secondaryToPin.get(pin);
		return secondary_id;
	}
	
	//get a Pin associated with this secondary_user_id
	public String getPinForSecondary(String secondary_user_id){
		
			String pin=null;
			if(pinToSecondaryMap.containsKey(secondary_user_id))
				pin=pinToSecondaryMap.get(secondary_user_id);
			return pin;
	}
	
	
	//Get the list of all UserId for this secondaryuserId
	public List<String> get(String secondaryuserId){
		
		List<String> users=new LinkedList<String>();
		
		for(String userId : Fun.filter(secondaryToUsers,secondaryuserId)){
			users.add(userId);
		}
		
		return users;
	}
	
	//add a user id for a secondary user id
	public void addUserToSecondary(String userID,String secondary_user_id){
		
		secondaryToUsers.add(Fun.t2(secondary_user_id,userID));
		
		

	}
	

}

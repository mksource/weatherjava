package com.solution.service;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class GenerateSecondaryId {
	
	public static Character randomStringrray[]={'a','1','b','3','c','5','d','7'};

	public String generateRandomId(String pin){
		
		
		//Generate random string and append pin should it will be unique for this user
		List<Character> randoms=Arrays.asList(randomStringrray);
		Collections.shuffle(randoms);
		StringBuilder builder=new StringBuilder("");
		for(Character c : randoms){
			builder.append(c);
		}
		String secondaryuserID=builder.toString()+pin;
		
		return secondaryuserID;
	
	}
}

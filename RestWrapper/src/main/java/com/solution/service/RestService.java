package com.solution.service;

import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;

import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import com.solution.dao.SecondaryPinDao;
import com.solution.exceptions.NotFoundException;
import com.solution.exceptions.ServerException;
import com.solution.util.ErrorMessages;

@Path("/pin")
public class RestService {
	
	
	//This methods will generate a secondar_user_id for a given pin 
	//This will create 
	@PUT
	@Path("/update_secondary_user")
	@Produces(MediaType.TEXT_PLAIN)
	public Response updateSecondaryUser(@QueryParam("id") String pin,@QueryParam("secondary_user") String secondaryUser){
		
		
		//Step 1. Check if the pin exists if it does not then throw an error
		//First check in the cache    
		RestInvoker invoker=new RestInvoker();
		CacheService service=CacheService.getCacheService();
		SecondaryPinDao dao=SecondaryPinDao.getSecondaryPinDao();
		GenerateSecondaryId id=new GenerateSecondaryId();
		
		
		String output=invoker.getPINInformation(pin);
		
		if(output == null){
			
			throw new NotFoundException(ErrorMessages.PIN_NOT_FOUND);
		}
		
		
		//Step 2. Check if secondary Id already exsists for this PIN
		String secondaryuserId=null;
		try {
			secondaryuserId = service.getSecondaryForPin(pin);
			
			
			if(secondaryuserId == null){
				
				secondaryuserId=id.generateRandomId(pin);
				
			
				
				//Step 4. Make a connection to the db to store the id of the pin
				dao.insertSecondaryId(pin, secondaryuserId);
				service.putSecondaryForPin(pin,secondaryuserId);
				
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new ServerException(ErrorMessages.SERVER_ERROR);
		}
		
		
		//Cache the secondary user is and 
		//put secondary user is as the key for list of user ids
		service.addUserToSecondary(secondaryUser, secondaryuserId);
			
		
		//Step 5. If successfully created then send 201 with Successfully created
		String result="User added as secondary user to PIN"+secondaryuserId;
		return Response.status(Status.CREATED).entity(result).build();
		
		
		
		
	}
	
	
	
	@GET
	@Path("/find")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getPIN(@Context UriInfo info){
		
				
		MultivaluedMap<String,String> map=info.getQueryParameters(true);
		CacheService service=CacheService.getCacheService();
		
		//if secondary_user_id is null then we need to fetch the PIN
		if(map.containsKey("id")){
			
			//fetch the PIN first
			String pin=map.getFirst("id");
			
			RestInvoker invoker=new RestInvoker();
			String pinData=invoker.getPINInformation(pin);
			if(pinData == null)
				throw new  NotFoundException(ErrorMessages.PIN_NOT_FOUND);
				
			JSONObject pinObj=(JSONObject) JSONValue.parse(pinData);
			
			pinObj.put("secondary_user_id",service.getSecondaryForPin(pin));
			
			return Response.status(Status.OK).entity(pinObj.toJSONString()).build();		
		}
		
		
		//If the pin is null then we need to fetch pin information based on the secondary user information
		else if(map.containsKey("secondar_user")){
			
			String secondary_user_id=map.getFirst("secondary_user");
			
			//Fetch the PIN for this secondary_user from the cache
			CacheService cache=CacheService.getCacheService();
			
			String pin=cache.getPinForSecondary(secondary_user_id);
			
			
			RestInvoker invoker=new RestInvoker();
			String pinData=invoker.getPINInformation(pin);
			if(pinData == null)
				throw new  NotFoundException(ErrorMessages.PIN_NOT_FOUND);
				
			JSONObject pinObj=(JSONObject) JSONValue.parse(pinData);
			
			pinObj.put("secondary_user_id",service.getSecondaryForPin(pin));
			pinObj.put("secondaryuser",service.get(secondary_user_id));
			
			System.out.println(pinObj.toJSONString());
			
			return Response.status(Status.OK).entity(pinObj.toJSONString()).build();
		}
		
		else
			return Response.status(Status.BAD_REQUEST).entity(ErrorMessages.PATH_PARAMETERS).build();
		
	}
	
	
	
	
	

}

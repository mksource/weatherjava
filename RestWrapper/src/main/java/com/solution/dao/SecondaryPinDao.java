package com.solution.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import com.solution.util.AppProperties;

public class SecondaryPinDao {
	
	private static SecondaryPinDao dao;
	
	private  SecondaryPinDao(){
		
	}
	
	//Constructor a singleton for a Dao
	public static SecondaryPinDao getSecondaryPinDao(){
		
		if(dao == null){
			synchronized(SecondaryPinDao.class){
				
				if(dao == null){
					
					dao=new SecondaryPinDao();
					
					
				}
			}
			
		}
		
		return dao;
	}
	
	//Fetch the secondary-user-id for given pin
	public boolean fetchSecondaryPIN(String pin) throws Exception{
		
		//Load the driver class 
		Class.forName("com.mysql.jdbc.Driver");
		
		Connection connect = null;
	    PreparedStatement statement = null;
	    
		 ResultSet resultSet = null;
		
		connect = DriverManager
		          .getConnection("jdbc:mysql://"+AppProperties.MYSQL_HOST_NAME+"/"+AppProperties.DATABASE_NAME+"?"
		              + "user="+AppProperties.USER_NAME+"&password="+AppProperties.USER_PASSWORD);
		
		try{
			
			statement=connect.prepareStatement("select secondary_user_id from  "+AppProperties.TABLE_NAME+" where pin_id = ?");
			statement.setInt(1, Integer.parseInt(pin));
			
			ResultSet set=statement.executeQuery();
			if(set.first()){
				
				return true;
			}
			
			return false;
			
		}
		finally{
			
			if(connect != null)
				connect.close();
		}
		
		
		
	}
	
	
	//Insert the secondary-user-id for given pin
	public void insertSecondaryId(String pin,String secondaryuser) throws Exception{
		
		//Load the driver class 
				Class.forName("com.mysql.jdbc.Driver");
				
				Connection connect = null;
			    PreparedStatement statement = null;
			    
				
				connect = DriverManager
				          .getConnection("jdbc:mysql://"+AppProperties.MYSQL_HOST_NAME+"/"+AppProperties.DATABASE_NAME+"?"
				              + "user="+AppProperties.USER_NAME+"&password="+AppProperties.USER_PASSWORD);
				
				try{
					
					statement=connect.prepareStatement("insert into "+AppProperties.TABLE_NAME+" (pin_id,field,value,secondary_user_id) values(?,?,?,?)");
					statement.setInt(1,Integer.parseInt(pin));
					statement.setString(2,"");
					statement.setString(3,"");
					statement.setString(4, secondaryuser);
					
					boolean isInserted=statement.execute();
					
				}
				finally{
					
					if(connect != null)
						connect.close();
				}
		
	}
	
	public static void main(String args[]){
		
		SecondaryPinDao dao=SecondaryPinDao.getSecondaryPinDao();
		try {
			dao.insertSecondaryId("366197","178");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	

}

package com.solution.util;

public class ErrorMessages {
	
	public static final String PIN_NOT_FOUND="{'error':'No pin found'}";
	public static final String SERVER_ERROR="There is some problem on Server will be right back";
	public static final String SECONDARY_USER_ID="{secondary user id is not found}";
	public static final String PATH_PARAMETERS="{please specify the proper path parameters}";

}

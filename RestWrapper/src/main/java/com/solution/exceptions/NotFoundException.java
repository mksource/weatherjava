package com.solution.exceptions;

import java.awt.PageAttributes.MediaType;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

public class NotFoundException extends WebApplicationException {

	public NotFoundException(String message){
		super(Response.status(Status.NOT_FOUND).entity(message).type(javax.ws.rs.core.MediaType.TEXT_PLAIN).build());
	}
}

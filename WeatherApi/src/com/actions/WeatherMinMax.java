package com.actions;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.services.WeatherApi;

/**
 * Servlet implementation class WeatherMinMax
 */
@WebServlet("/getMinMax")
public class WeatherMinMax extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public WeatherMinMax() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
String cityName=request.getParameter("cityName");
		
		WeatherApi api=new WeatherApi();
		
		String weatherData =api.fetchForecast(cityName);
		
		
		//Set the response
		if(weatherData == null){
			
			response.setStatus(HttpServletResponse.SC_FORBIDDEN);
		}
		else{
			
			response.setHeader("Conent-Type","application/json");
			PrintWriter writer=response.getWriter();
			response.setStatus(HttpServletResponse.SC_OK);
			writer.print(weatherData);
			
		}	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}

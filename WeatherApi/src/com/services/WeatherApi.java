package com.services;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class WeatherApi {
	
	
	
	
	
	//Fetch the forecast for particular city
	public String fetchForecast(String cityName){
		
		String url="http://api.openweathermap.org/data/2.5/forecast?q="+cityName+"&mode=json&units=metric";
		
		String response=this.fetchURL(url);
		
		//send back the response 	
		
		System.out.println(response);
		
		return response;
		
	}
	
	
	//Fetch the minimum and maximum temperature for particular city
	public String fetchMinMax(String cityName){
		
		String url="http://api.openweathermap.org/data/2.5/forecast/daily?q="+cityName+"&mode=json&units=metric&cnt=1";
		
		String response=this.fetchURL(url);
		
		System.out.println(response);
		
		//send back the response
		
		return response;
		
	}
	
	public String fetchURL(String urlstring){
		
		String output=null;
		
		try {
			 
			URL url = new URL(urlstring);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");
	 
			if (conn.getResponseCode() != 200) {
				
				output=null;
			}
	 
			BufferedReader br = new BufferedReader(new InputStreamReader(
				(conn.getInputStream())));
	 
			StringBuffer buf=new StringBuffer("");
			String line=null;
			System.out.println("Output from Server .... \n");
			while ((line = br.readLine()) != null) {
				buf.append(line);
			}
	 
			output=buf.toString();
			
			conn.disconnect();
	 
		  } catch (MalformedURLException e) {
	 
			e.printStackTrace();
	 
		  } catch (IOException e) {
	 
			e.printStackTrace();
	 
		  }
		
		return output;
	}

}

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<script type="text/javascript" src="index.js"></script>
<script type="text/javascript" src="Chart.min.js"></script>
<script type="text/javascript" src="jquery-1.11.3.min.js"></script>
<script>
$(document).ready(function(){
	$("#minmaxdisplay").hide();
});
</script>
</head>
<body>
<h2 style="color:#F5A91C">Weather in your city</h2><br/><br/>
<div id="form">
	<b>Enter your City Name:</b>
	<input type="text" id="city" autocomplete="on"></input><br/><br/>
	<b>Select Display option:</b>
	<select id="displayoptions">
		<option id="opt01">Min-Max</option>
		<option id="opt02">Forecast</option>
	</select><br/><br/>
	
	
	<input type="submit" onclick="doSubmit()"/>
</div>
<br/><br/>
<div id="minmaxdisplay">
		<b>Minimum Temperature Today is:</b>
		<p id="min"></p>
		<br/>
		<b>Maximum Temperature Today is:</b>
		<p id="max"></p>
</div>
<canvas id="myChart" width="800" height="400"></canvas>
</body>

</html>
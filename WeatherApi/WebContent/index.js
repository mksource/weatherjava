/**
 * 
 */
function doSubmit(){
	
	var cityName=document.getElementById("city").value;
	var optionName=document.getElementById("displayoptions").selectedIndex;
	
	if(cityName == ""){
		alert("City Name is blank");
		return;
	}
	
	$("#minmaxdisplay").hide();
	
	if(optionName == 0){
		
		doGetMinMax(cityName);
	}
	else if (optionName == 1) {
		
		doGetDay(cityName);
	}
	
	
}
function doGetDay(cityName){
	
	var jsonurl="getForcast?cityName="+cityName;
	$.get(jsonurl,function(data,status){
		
		 alert(data);
		  if(status == "success"){
			  doPlot(data);
			}
			else{
				alert("Loading Weather data failed")
			}
	});
	
	
}
function doGetMinMax(cityName){
	
	var jsonurl="getMinMax?cityName="+cityName;
	$.get(jsonurl,function(data,status){
		
		
		alert(data);
		if(status == "success"){
			$("#min").text(data.list[0].temp.min);
			$("#max").text(data.list[0].temp.max);
			$("#minmaxdisplay").show();
		}
		else{
			alert("Loading Weather data failed")
		}
	
	});
}
function doPlot(data){
	
	var temperature=[];
	var time=[];
	for(i=0;i<data.list.length;i++){
		
		temperature.push(data.list[i].main.temp);
		time.push(data.list[i].dt_txt);
	}
	
	var ctx=$("#myChart").get(0).getContext("2d");
	var myNewChart=new Chart(ctx);
	
	var data = {
		    labels: time,
		    datasets: [
		        {
		            label: "7 day Forecast",
		            fillColor: "rgba(220,220,220,0.2)",
		            strokeColor: "rgba(220,220,220,1)",
		            pointColor: "rgba(220,220,220,1)",
		            pointStrokeColor: "#fff",
		            pointHighlightFill: "#fff",
		            pointHighlightStroke: "rgba(220,220,220,1)",
		            data: temperature
		        }]};
	
	var myLineChart=new Chart(ctx).Line(data);
	
}